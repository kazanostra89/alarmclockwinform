﻿namespace AlarmClock
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.labelClock = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAddList = new System.Windows.Forms.Button();
            this.listBoxAlarms = new System.Windows.Forms.ListBox();
            this.buttonRemoveList = new System.Windows.Forms.Button();
            this.buttonClearList = new System.Windows.Forms.Button();
            this.numUpDownMinutes = new System.Windows.Forms.NumericUpDown();
            this.numUpDownSeconds = new System.Windows.Forms.NumericUpDown();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonPause = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownMinutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownSeconds)).BeginInit();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // labelClock
            // 
            this.labelClock.AutoSize = true;
            this.labelClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelClock.Location = new System.Drawing.Point(96, 16);
            this.labelClock.Name = "labelClock";
            this.labelClock.Size = new System.Drawing.Size(66, 25);
            this.labelClock.TabIndex = 0;
            this.labelClock.Text = "00:00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(120, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 31);
            this.label1.TabIndex = 3;
            this.label1.Text = ":";
            // 
            // buttonAddList
            // 
            this.buttonAddList.Location = new System.Drawing.Point(64, 88);
            this.buttonAddList.Name = "buttonAddList";
            this.buttonAddList.Size = new System.Drawing.Size(136, 24);
            this.buttonAddList.TabIndex = 4;
            this.buttonAddList.Text = "Добавить будильник";
            this.buttonAddList.UseVisualStyleBackColor = true;
            this.buttonAddList.Click += new System.EventHandler(this.buttonAddList_Click);
            // 
            // listBoxAlarms
            // 
            this.listBoxAlarms.FormattingEnabled = true;
            this.listBoxAlarms.Location = new System.Drawing.Point(24, 120);
            this.listBoxAlarms.Name = "listBoxAlarms";
            this.listBoxAlarms.Size = new System.Drawing.Size(120, 95);
            this.listBoxAlarms.TabIndex = 5;
            // 
            // buttonRemoveList
            // 
            this.buttonRemoveList.Enabled = false;
            this.buttonRemoveList.Location = new System.Drawing.Point(16, 224);
            this.buttonRemoveList.Name = "buttonRemoveList";
            this.buttonRemoveList.Size = new System.Drawing.Size(136, 24);
            this.buttonRemoveList.TabIndex = 6;
            this.buttonRemoveList.Text = "Удалить будильник";
            this.buttonRemoveList.UseVisualStyleBackColor = true;
            this.buttonRemoveList.Click += new System.EventHandler(this.buttonRemoveList_Click);
            // 
            // buttonClearList
            // 
            this.buttonClearList.Enabled = false;
            this.buttonClearList.Location = new System.Drawing.Point(16, 256);
            this.buttonClearList.Name = "buttonClearList";
            this.buttonClearList.Size = new System.Drawing.Size(136, 24);
            this.buttonClearList.TabIndex = 7;
            this.buttonClearList.Text = "Очистить записи";
            this.buttonClearList.UseVisualStyleBackColor = true;
            this.buttonClearList.Click += new System.EventHandler(this.buttonClearList_Click);
            // 
            // numUpDownMinutes
            // 
            this.numUpDownMinutes.Location = new System.Drawing.Point(80, 56);
            this.numUpDownMinutes.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numUpDownMinutes.Name = "numUpDownMinutes";
            this.numUpDownMinutes.ReadOnly = true;
            this.numUpDownMinutes.Size = new System.Drawing.Size(40, 20);
            this.numUpDownMinutes.TabIndex = 8;
            // 
            // numUpDownSeconds
            // 
            this.numUpDownSeconds.Location = new System.Drawing.Point(144, 56);
            this.numUpDownSeconds.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numUpDownSeconds.Name = "numUpDownSeconds";
            this.numUpDownSeconds.ReadOnly = true;
            this.numUpDownSeconds.Size = new System.Drawing.Size(40, 20);
            this.numUpDownSeconds.TabIndex = 9;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(160, 128);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(80, 24);
            this.buttonStart.TabIndex = 10;
            this.buttonStart.Text = "Старт";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonPause
            // 
            this.buttonPause.Enabled = false;
            this.buttonPause.Location = new System.Drawing.Point(160, 160);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(80, 24);
            this.buttonPause.TabIndex = 11;
            this.buttonPause.Text = "Пауза";
            this.buttonPause.UseVisualStyleBackColor = true;
            this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Enabled = false;
            this.buttonReset.Location = new System.Drawing.Point(160, 192);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(80, 24);
            this.buttonReset.TabIndex = 12;
            this.buttonReset.Text = "Сброс";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 293);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonPause);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.numUpDownSeconds);
            this.Controls.Add(this.numUpDownMinutes);
            this.Controls.Add(this.buttonClearList);
            this.Controls.Add(this.buttonRemoveList);
            this.Controls.Add(this.listBoxAlarms);
            this.Controls.Add(this.buttonAddList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelClock);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Будильник";
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownMinutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownSeconds)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label labelClock;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAddList;
        private System.Windows.Forms.ListBox listBoxAlarms;
        private System.Windows.Forms.Button buttonRemoveList;
        private System.Windows.Forms.Button buttonClearList;
        private System.Windows.Forms.NumericUpDown numUpDownMinutes;
        private System.Windows.Forms.NumericUpDown numUpDownSeconds;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.Button buttonReset;
    }
}

