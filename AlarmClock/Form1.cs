﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmClock
{
    public partial class Form1 : Form
    {
        private int counterTime;

        public Form1()
        {
            InitializeComponent();

            counterTime = 0;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            counterTime++;

            int minutes = (counterTime % 3600) / 60;
            int seconds = counterTime % 60;

            labelClock.Text = minutes.ToString("00") + ":" + seconds.ToString("00");

            if (listBoxAlarms.Items.Contains(labelClock.Text))
            {
                MessageBox.Show("Сработал на отметке: " + labelClock.Text);
            }

        }

        private void buttonAddList_Click(object sender, EventArgs e)
        {
            string minutes = numUpDownMinutes.Value.ToString("00");
            string seconds = numUpDownSeconds.Value.ToString("00");

            listBoxAlarms.Items.Add(String.Concat(minutes, ":", seconds));

            if (!buttonRemoveList.Enabled && !buttonClearList.Enabled)
            {
                buttonRemoveList.Enabled = true;
                buttonClearList.Enabled = true;
            }
        }

        private void buttonClearList_Click(object sender, EventArgs e)
        {
            listBoxAlarms.Items.Clear();

            CheckZeroListItemsAlarm();
        }

        private void buttonRemoveList_Click(object sender, EventArgs e)
        {
            object obj = listBoxAlarms.SelectedItem;

            if (obj == null)
            {
                MessageBox.Show("Элемент не выбран!");
                return;
            }

            listBoxAlarms.Items.Remove(obj);

            listBoxAlarms.SelectedItem = null;

            CheckZeroListItemsAlarm();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            timer.Start();

            buttonStart.Enabled = false;
            buttonPause.Enabled = true;
            buttonReset.Enabled = true;
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            timer.Stop();

            counterTime = 0;

            if (buttonPause.Text.Contains("Продолжить"))
            {
                buttonPause.Text = "Пауза";
            }

            labelClock.Text = "00:00";

            buttonStart.Enabled = true;
            buttonPause.Enabled = false;
            buttonReset.Enabled = false;
        }

        private void buttonPause_Click(object sender, EventArgs e)
        {
            if (buttonPause.Text == "Продолжить")
            {
                timer.Start();

                buttonPause.Text = "Пауза";

                return;
            }

            timer.Stop();

            buttonPause.Text = "Продолжить";
        }

        private void CheckZeroListItemsAlarm()
        {
            if (listBoxAlarms.Items.Count == 0)
            {
                buttonRemoveList.Enabled = false;
                buttonClearList.Enabled = false;
            }
        }


    }
}
